import {useState, useEffect} from 'react';
import { Card, Button, } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function CourseCard({course}) {

	const { name, description, price, _id } = course;

	// count - getter
	// setCount - setter
	// useState(0) - useState(initialGetterValue) 
	// const [count, setCount] = useState(0);
	// S51 ACTIVITY 
	// const [seats, setSeats] = useState(5);
	// S51 ACTIVITY END
	//console.log(useState(0));
	// const [isOpen, setIsOpen] = useState(true);

	// function that keeps track of the enrolles for a course
	function enroll() {
		// setCount(count + 1);
		// console.log('Enrollees: ' + count);
	// S51 ACTIVITY 
		// setSeats(seats - 1);
		// console.log('Seats: ' + seats);

		// if(seats === 1){
		// 	alert("No more seats.")
		// 	document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true);
		// }
	// S51 ACTIVITY END
	}

	// useEffect allows us to execute function if the value of seats state changes.
	// useEffect(() => {
	// 	if(seats === 0){
	// 	setIsOpen(false);
	// 	alert("No more seats.")
	// 	document.querySelector(`#btn-enroll-${_id}`).setAttribute('disabled', true);
	// 	}
	// 	// will run anytime one of the values in the array of dependencies changes.
	// }, [seats])

	

	// Checks to see if the data was succesfully passed.
	// console.log(props);
	// Every component receives information in a form of an object
	// console.log(typeof props);

	return (
		
			<Card class="text-center" md={3}>
			    <Card.Body>
			        <Card.Title class="text-center">{name}</Card.Title>
			        <Card.Subtitle Class="text-center">Description:</Card.Subtitle>
			        <Card.Text class="text-center">{description}</Card.Text>
			        <Card.Subtitle class="text-center">Price:</Card.Subtitle>
			        <Card.Text class="text-center">PhP {price}</Card.Text>
			        <div class="text-center">
			        <Button className="bg-primary" as={Link} to={`/courses/${_id}`} >Checkout</Button>
			        </div>
			    </Card.Body>
			</Card>
		
	)	
};

// "proptypes" - are a good way of checking data type of information between components.
CourseCard.propTypes = {
	// "shape" method is used to check if prop object conforms to a specific "shape"
	course: PropTypes.shape({
		// Defined properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
