// import Button from 'react-bootstrap/Button';
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';

import { Button, Row, Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}) {

    const {title, content, destination, label} = data;

return (

    <Col xs={12} md={4} class="text-center">
        <Card className="cardHighlight p-3">
            <Card.Body>
                <Card.Title>
                    <h4 class="text-center">{title}</h4>
                </Card.Title>
                <Card.Text class="text-center"> 
                    {content}
                </Card.Text>
                <div class="text-center">
                <Button variant="primary" as={Link} to={destination}>{label}</Button>
                </div>
            </Card.Body>
        </Card>
    </Col>
    // <Row class="banner">
    //     <Col className="p-5">
    //         <h1>{title}</h1>
    //         <p>{content}</p>
    //         <Button variant="primary" as={Link} to={destination}>{label}</Button>
    //     </Col>
    // </Row>
    )
}


{/*<Col xs={12} md={6}>
    <Card className="cardHighlight p-3">
        <Card.Body>
            <Card.Title>
                <h4 class="text-center">{title}</h4>
            </Card.Title> <br/>
            <Card.Text class="text-center"> 
                {content}
            </Card.Text>
            <Button variant="primary" as={Link} to={destination}>{label}</Button>
        </Card.Body>
    </Card>
</Col>*/}